package danielmg.vr.jovr.api.data;

public class RectI {

	private Vector2i position;
	private SizeI size;
	
	public RectI() {}
	
	/**
	 * @param position
	 * @param size
	 */
	public RectI(Vector2i position, SizeI size) {
		this.position = position;
		this.size = size;
	}
	public Vector2i getPosition() {
		return position;
	}
	public void setPosition(Vector2i position) {
		this.position = position;
	}
	public SizeI getSize() {
		return size;
	}
	public void setSize(SizeI size) {
		this.size = size;
	}
	
	
	
	
}
