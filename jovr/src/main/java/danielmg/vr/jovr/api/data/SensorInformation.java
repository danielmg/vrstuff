package danielmg.vr.jovr.api.data;

public class SensorInformation {

	private Vector3f accelerometer=new Vector3f();
	private Vector3f gyro=new Vector3f();
	private Vector3f magnetometer=new Vector3f();
	
	private float temperature=0f;
	private float timeInSeconds=0f;
	
	private Vector3f angularAcceleration=new Vector3f();
	private Vector3f angularVelocity=new Vector3f();
	private Vector3f linearAcceleration=new Vector3f();
	private Vector3f linearVelocity=new Vector3f();
	
	double poseTimeInSeconds=0d;
	
	private Vector4f headOrientation= new Vector4f();
	private Vector3f headPosition =new Vector3f();
	
	private Vector4f cameraOrientation= new Vector4f();
	private Vector3f cameraPosition =new Vector3f();
	
	private Vector4f leveledCameraOrientation= new Vector4f();
	private Vector3f leveledCameraPosition =new Vector3f();
	
	private long pcTimeInMillis;
	
	private long statusFlags;
	
	public double getPoseTimeInSeconds() {
		return poseTimeInSeconds;
	}
	public void setPoseTimeInSeconds(double poseTimeInSeconds) {
		this.poseTimeInSeconds = poseTimeInSeconds;
	}
	public Vector3f getAccelerometer() {
		return accelerometer;
	}
	public Vector3f getGyro() {
		return gyro;
	}
	public Vector3f getMagnetometer() {
		return magnetometer;
	}
	public Vector3f getAngularAcceleration() {
		return angularAcceleration;
	}
	public Vector3f getAngularVelocity() {
		return angularVelocity;
	}
	public Vector3f getLinearAcceleration() {
		return linearAcceleration;
	}
	public Vector3f getLinearVelocity() {
		return linearVelocity;
	}
	public Vector4f getHeadOrientation() {
		return headOrientation;
	}
	public Vector3f getHeadPosition() {
		return headPosition;
	}
	public float getTimeInSeconds() {
		return timeInSeconds;
	}
	public void setTimeInSeconds(float timeInSeconds) {
		this.timeInSeconds = timeInSeconds;
	}
	public float getTemperature() {
		return temperature;
	}
	public void setTemperature(float temperature) {
		this.temperature = temperature;
	}
	public long getStatusFlags() {
		return statusFlags;
	}
	public void setStatusFlags(long statusFlags) {
		this.statusFlags = statusFlags;
	}
	public Vector4f getCameraOrientation() {
		return cameraOrientation;
	}
	public Vector3f getCameraPosition() {
		return cameraPosition;
	}
	public Vector4f getLeveledCameraOrientation() {
		return leveledCameraOrientation;
	}
	public Vector3f getLeveledCameraPosition() {
		return leveledCameraPosition;
	}
	public long getPcTimeInMillis() {
		return pcTimeInMillis;
	}
	public void setPcTimeInMillis(long pcTimeInMillis) {
		this.pcTimeInMillis = pcTimeInMillis;
	}
	@Override
	public String toString() {
		return "SensorInformation [accelerometer=" + accelerometer + ", gyro="
				+ gyro + ", magnetometer=" + magnetometer + ", temperature="
				+ temperature + ", timeInSeconds=" + timeInSeconds
				+ ", angularAcceleration=" + angularAcceleration
				+ ", angularVelocity=" + angularVelocity
				+ ", linearAcceleration=" + linearAcceleration
				+ ", linearVelocity=" + linearVelocity + ", poseTimeInSeconds="
				+ poseTimeInSeconds + ", headOrientation=" + headOrientation
				+ ", headPosition=" + headPosition + ", cameraOrientation="
				+ cameraOrientation + ", cameraPosition=" + cameraPosition
				+ ", leveledCameraOrientation=" + leveledCameraOrientation
				+ ", leveledCameraPosition=" + leveledCameraPosition
				+ ", pcTimeInMillis=" + pcTimeInMillis + ", statusFlags="
				+ statusFlags + "]";
	}
	
	
	
	
}
