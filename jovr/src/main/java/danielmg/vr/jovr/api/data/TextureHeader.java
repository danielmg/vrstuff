package danielmg.vr.jovr.api.data;

public class TextureHeader {

	private SizeI textureSize;
	private RectI renderViewport;
	private RenderAPIType apiType;
	
	public TextureHeader(){}
	
	/**
	 * @param textureSizeI
	 * @param renderViewport
	 * @param apiType
	 */
	public TextureHeader(SizeI textureSize, RectI renderViewport,
			RenderAPIType apiType) {
		this.textureSize = textureSize;
		this.renderViewport = renderViewport;
		this.apiType = apiType;
	}
	public SizeI getTextureSize() {
		return textureSize;
	}
	public void setTextureSize(SizeI textureSize) {
		this.textureSize = textureSize;
	}
	public RectI getRenderViewport() {
		return renderViewport;
	}
	public void setRenderViewport(RectI renderViewport) {
		this.renderViewport = renderViewport;
	}
	public RenderAPIType getApiType() {
		return apiType;
	}
	public void setApiType(RenderAPIType apiType) {
		this.apiType = apiType;
	}
	
	
	
}
