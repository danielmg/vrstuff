package danielmg.vr.jovr.api.data;

public class Vector4f {

	private float x;
	private float y;
	private float z;
	private float w;
	
	
	public float getX() {
		return x;
	}
	public float getY() {
		return y;
	}
	public float getZ() {
		return z;
	}
	
	public float getW() {
		return w;
	}
	public void setX(float x) {
		this.x = x;
	}
	public void setY(float y) {
		this.y = y;
	}
	public void setZ(float z) {
		this.z = z;
	}
	public void setW(float w) {
		this.w = w;
	}
	@Override
	public String toString() {
		return "Vector4f [x=" + x + ", y=" + y + ", z=" + z + ", w=" + w + "]";
	}
	
	
	
}
