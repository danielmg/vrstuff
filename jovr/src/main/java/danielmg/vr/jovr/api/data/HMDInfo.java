package danielmg.vr.jovr.api.data;

public class HMDInfo {

	private String manufacturer;
	private String productName;
	private String serialNumber;
	private String firmwareVersion;
	private String deviceType;
	private int resolutionX;
	private int resolutionY;
	private String displayDeviceName;
	
	
	private float cameraFrustumFarZInMeters;
	private float cameraFrustumHFovInRadians;
	private float cameraFrustumNearZInMeters;
	private float cameraFrustumVFovInRadians;
	
	private long trackingCaps;
	
	
	
	
	
	
	public HMDInfo(String manufacturer, String productName,
			String serialNumber, String firmwareVersion, String deviceType,
			int resolutionX, int resolutionY, String displayDeviceName,
			float cameraFrustumFarZInMeters, float cameraFrustumHFovInRadians,
			float cameraFrustumNearZInMeters, float cameraFrustumVFovInRadians,
			long trackingCaps) {
		super();
		this.manufacturer = manufacturer;
		this.productName = productName;
		this.serialNumber = serialNumber;
		this.firmwareVersion = firmwareVersion;
		this.deviceType = deviceType;
		this.resolutionX = resolutionX;
		this.resolutionY = resolutionY;
		this.displayDeviceName = displayDeviceName;
		this.cameraFrustumFarZInMeters = cameraFrustumFarZInMeters;
		this.cameraFrustumHFovInRadians = cameraFrustumHFovInRadians;
		this.cameraFrustumNearZInMeters = cameraFrustumNearZInMeters;
		this.cameraFrustumVFovInRadians = cameraFrustumVFovInRadians;
		this.trackingCaps = trackingCaps;
	}
	public int getResolutionX() {
		return resolutionX;
	}
	public int getResolutionY() {
		return resolutionY;
	}
	public long getTrackingCaps() {
		return trackingCaps;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public String getProductName() {
		return productName;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public String getFirmwareVersion() {
		return firmwareVersion;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public String getDisplayDeviceName() {
		return displayDeviceName;
	}
	public float getCameraFrustumFarZInMeters() {
		return cameraFrustumFarZInMeters;
	}
	public float getCameraFrustumHFovInRadians() {
		return cameraFrustumHFovInRadians;
	}
	public float getCameraFrustumNearZInMeters() {
		return cameraFrustumNearZInMeters;
	}
	public float getCameraFrustumVFovInRadians() {
		return cameraFrustumVFovInRadians;
	}
	
	
	
	
	
}
