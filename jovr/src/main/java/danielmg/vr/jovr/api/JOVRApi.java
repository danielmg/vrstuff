package danielmg.vr.jovr.api;

import danielmg.vr.jovr.api.data.HMDInfo;
import danielmg.vr.jovr.api.data.Matrix4f;
import danielmg.vr.jovr.api.data.RectI;
import danielmg.vr.jovr.api.data.RenderAPIType;
import danielmg.vr.jovr.api.data.SensorInformation;
import danielmg.vr.jovr.api.data.SizeI;
import danielmg.vr.jovr.api.data.Texture;
import danielmg.vr.jovr.api.data.TextureHeader;
import danielmg.vr.jovr.api.data.Vector2i;
import danielmg.vr.jovr.jni.OvrCapi;
import danielmg.vr.jovr.jni.ovrEyeType;
import danielmg.vr.jovr.jni.ovrFovPort;
import danielmg.vr.jovr.jni.ovrHmdDesc;
import danielmg.vr.jovr.jni.ovrMatrix4f;
import danielmg.vr.jovr.jni.ovrSizei;
import danielmg.vr.jovr.jni.ovrVector2f;

/**
 * @author Daniel
 *
 */
public final class JOVRApi {

	//From the LibOVR headers:
	
	// -----------------------------------------------------------------------------------
	// ***** API Interfaces

	// Basic steps to use the API:
	//
	// Setup:
	//  1. ovrInitialize()
	//  2. ovrHMD hmd = ovrHmd_Create(0)
	//  3. Use hmd members and ovrHmd_GetFovTextureSize() to determine graphics configuration.
	//  4. Call ovrHmd_ConfigureTracking() to configure and initialize tracking.
	//  5. Call ovrHmd_ConfigureRendering() to setup graphics for SDK rendering,
//	     which is the preferred approach.
//	     Please refer to "Client Distorton Rendering" below if you prefer to do that instead.
	//  6. If the ovrHmdCap_ExtendDesktop flag is not set, then use ovrHmd_AttachToWindow to 
//	     associate the relevant application window with the hmd.
	//  5. Allocate render target textures as needed.
	//
	// Game Loop:
	//  6. Call ovrHmd_BeginFrame() to get the current frame timing information.
	//  7. Render each eye using ovrHmd_GetEyePose to get the predicted head pose.
	//  8. Call ovrHmd_EndFrame() to render the distorted textures to the back buffer
//	     and present them on the hmd.
	//
	// Shutdown:
	//  9. ovrHmd_Destroy(hmd)
	//  10. ovr_Shutdown()
	//
	
	private ovrHmdDesc hmd;
	
	private SensorPollerThread spt;
	
	private Texture[] eyeTextures = new Texture[]{new Texture(),new Texture()};
	private Matrix4f[] projections;
	
	private RenderAPIType apiType;

	private static JOVRApi instance;
	
	private int configureTracking;
	
	//the reason I used my own API is so I don't have to change anything built on top if the Oculus implementation changes. 
	
	private JOVRApi(){
		
		//TODO:clean this up
		if (System.getProperty("os.name").startsWith("Windows")) {
			if (System.getProperty("sun.arch.data.model").equals("32")){
				System.load(ClassLoader.getSystemResource("LibOVRDll.dll").getFile());
			} else if (System.getProperty("sun.arch.data.model").equals("64")) {
				System.load(ClassLoader.getSystemResource("LibOVRDll_x64.dll").getFile());
			} else {
				throw new RuntimeException("unknown architecture: " + System.getProperty("sun.arch.data.model"));
			}
		} else if (System.getProperty("os.name").startsWith("Windows")) {
			if (System.getProperty("sun.arch.data.model").equals("32")){
				System.load(ClassLoader.getSystemResource("LibOVRDll.so").getFile());
			} else if (System.getProperty("sun.arch.data.model").equals("64")) {
				System.load(ClassLoader.getSystemResource("LibOVRDll_x64.so").getFile());
			} else {
				throw new RuntimeException("unknown architecture: " + System.getProperty("sun.arch.data.model"));
			}
		} else {
			throw new RuntimeException("unknown OS: " + System.getProperty("os.name"));
		}
		
		
		OvrCapi.ovr_Initialize();
		this.hmd=OvrCapi.ovrHmd_Create(0);
		
		
		//configure tracking
		this.configureTracking= (int)OvrCapi.ovrHmd_ConfigureTracking(hmd, hmd.getTrackingCaps(), 0);
		
		//start our poller
		this.spt=new SensorPollerThread(hmd);		
		new Thread(spt).start();
		
	}
	
	
	/**
	 * @param apiType
	 * @return instance of JOVRApi and initialise the graphics configuration if being used
	 */
	public synchronized static JOVRApi getInstance(RenderAPIType apiType) {
		if (instance==null) {
			instance=new JOVRApi();
			instance.apiType=apiType;
			
			//only bother initialising graphics if we are rendering
			if (apiType!=RenderAPIType.None)  instance.initialiseGraphicsSetup();
			
		} else {
			//Doh - you can't have two render types at once
			if (apiType!=instance.apiType) 
				throw new RuntimeException("Renderer already configured for: " + instance.apiType + " please shutdown before trying to init with: " + apiType);
		}
		
		return instance;
	}
	
	/**
	 * shuts everything down and clears the instance
	 */
	public synchronized void shutdown() {
		//stop the poller
		spt.shutdown();
		//tell API to shutdown
		OvrCapi.ovrHmd_Destroy(hmd);
		OvrCapi.ovr_Shutdown();
		instance=null;
	}
	
	private void initialiseGraphicsSetup() {
		this.projections=new Matrix4f[2];
		
		ovrFovPort[] fovPorts = this.getFOVPorts();
		
		 for (int eye = 0; eye < 2; ++eye) {
		
			 ovrMatrix4f om4f =this.getPerspectiveProjection(fovPorts[eye], 0.1f, 1000000f, true);
			 
			projections[eye] = new Matrix4f();
			
			//horrible conversion between multi-dimensional arrays in C++ and Java - need to learn how to get SWIG to do this better
			for (int x=0;x<4;x++) for (int y=0;y<4;y++) projections[eye].getMatrix()[x][y]=om4f.getM(x, y); 
			 
		      Texture texture = eyeTextures[eye];
		      
		      ovrSizei i = OvrCapi.ovrHmd_GetFovTextureSize(hmd, eye==0?ovrEyeType.ovrEye_Left:ovrEyeType.ovrEye_Right,  fovPorts[eye], 1.0f);
		      SizeI textureSize = new SizeI(i.getW(), i.getH());

		      texture.setTextureHeader(new TextureHeader(textureSize, new RectI(new Vector2i(0,0), textureSize), apiType));		      
	    }
			
	}
	
	private ovrMatrix4f getPerspectiveProjection(ovrFovPort fovPort, float zNear, float zFar, boolean rightHanded) {
		
		return OvrCapi.ovrMatrix4f_Projection(fovPort, zNear, zFar, (char)(rightHanded?1:0));
		
	}
	
	private ovrMatrix4f getOrthographicProjection(ovrMatrix4f projection, ovrVector2f orthographicScale, float orthographicProjectionDistance, float eyeViewAdjustX ) {
		
		return OvrCapi.ovrMatrix4f_OrthoSubProjection(projection, orthographicScale, orthographicProjectionDistance, eyeViewAdjustX);
	
	}
	
	private ovrFovPort[] getFOVPorts() {
		return hmd.getDefaultEyeFov();
	}
	
	public SensorInformation getSensorInformation() {
		return this.spt.getSensorInformation();
	}
	

	public int getConfigureTracking() {
		return configureTracking;
	}
	

	public HMDInfo getHMDInfo() {
		
		return new HMDInfo(hmd.getManufacturer(), hmd.getProductName(), hmd.getSerialNumber(), hmd.getFirmwareMajor()+"." + hmd.getFirmwareMinor(), hmd.getType().toString(), 
				hmd.getResolution().getW() ,hmd.getResolution().getH(), hmd.getDisplayDeviceName(),hmd.getCameraFrustumFarZInMeters(),hmd.getCameraFrustumHFovInRadians(),
				hmd.getCameraFrustumNearZInMeters(),hmd.getCameraFrustumVFovInRadians(),hmd.getTrackingCaps());
		
	}
	
}
