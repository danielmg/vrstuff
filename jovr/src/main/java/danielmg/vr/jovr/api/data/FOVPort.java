package danielmg.vr.jovr.api.data;

public class FOVPort {
	
	private float upTan;
	private float downTan;
	private float leftTan;
	private float rightTan;
	
	public FOVPort(){}
	
	/**
	 * @param upTan
	 * @param downTan
	 * @param leftTan
	 * @param rightTan
	 */
	public FOVPort(float upTan, float downTan, float leftTan, float rightTan) {
		this.upTan = upTan;
		this.downTan = downTan;
		this.leftTan = leftTan;
		this.rightTan = rightTan;
	}
	public float getUpTan() {
		return upTan;
	}
	public void setUpTan(float upTan) {
		this.upTan = upTan;
	}
	public float getDownTan() {
		return downTan;
	}
	public void setDownTan(float downTan) {
		this.downTan = downTan;
	}
	public float getLeftTan() {
		return leftTan;
	}
	public void setLeftTan(float leftTan) {
		this.leftTan = leftTan;
	}
	public float getRightTan() {
		return rightTan;
	}
	public void setRightTan(float rightTan) {
		this.rightTan = rightTan;
	}
	
	

}
