package danielmg.vr.jovr.api;

import danielmg.vr.jovr.api.data.SensorInformation;
import danielmg.vr.jovr.jni.OvrCapi;
import danielmg.vr.jovr.jni.ovrHmdDesc;
import danielmg.vr.jovr.jni.ovrPoseStatef;
import danielmg.vr.jovr.jni.ovrPosef;
import danielmg.vr.jovr.jni.ovrSensorData;
import danielmg.vr.jovr.jni.ovrTrackingState;

public class SensorPollerThread implements Runnable {

	private static final long POLL_INTERVAL=5; //ms
	
	private boolean run=Boolean.TRUE;;
	private ovrHmdDesc hmd;
	private SensorInformation sensorInformation  =new SensorInformation(); 
	
	SensorPollerThread(ovrHmdDesc hmd){
		this.hmd=hmd;
	}
	
	@Override
	public void run() {

		while(run) {
			//poll away
			ovrTrackingState  state=  OvrCapi.ovrHmd_GetTrackingState(hmd, 1);
			
			//instead of creating fresh vectors from my API each time i'm just copying the primitives across to save on instantiation and GC

			ovrSensorData sensorData = state.getRawSensorData();

			this.sensorInformation.getAccelerometer().setX(sensorData.getAccelerometer().getX());
			this.sensorInformation.getAccelerometer().setY(sensorData.getAccelerometer().getY());
			this.sensorInformation.getAccelerometer().setZ(sensorData.getAccelerometer().getZ());
			
			this.sensorInformation.getGyro().setX(sensorData.getGyro().getX());
			this.sensorInformation.getGyro().setY(sensorData.getGyro().getY());
			this.sensorInformation.getGyro().setZ(sensorData.getGyro().getZ());
			
			this.sensorInformation.getMagnetometer().setX(sensorData.getMagnetometer().getX());
			this.sensorInformation.getMagnetometer().setY(sensorData.getMagnetometer().getY());
			this.sensorInformation.getMagnetometer().setZ(sensorData.getMagnetometer().getZ());
			
			this.sensorInformation.setTemperature(sensorData.getTemperature());
			this.sensorInformation.setTimeInSeconds(sensorData.getTimeInSeconds());
			
			ovrPoseStatef headPose=state.getHeadPose();
			
			this.sensorInformation.getAngularAcceleration().setX(headPose.getAngularAcceleration().getX());
			this.sensorInformation.getAngularAcceleration().setY(headPose.getAngularAcceleration().getY());
			this.sensorInformation.getAngularAcceleration().setZ(headPose.getAngularAcceleration().getZ());
			
			this.sensorInformation.getAngularVelocity().setX(headPose.getAngularVelocity().getX());
			this.sensorInformation.getAngularVelocity().setY(headPose.getAngularVelocity().getY());
			this.sensorInformation.getAngularVelocity().setZ(headPose.getAngularVelocity().getZ());
			
			
			this.sensorInformation.getLinearAcceleration().setX(headPose.getLinearAcceleration().getX());
			this.sensorInformation.getLinearAcceleration().setY(headPose.getLinearAcceleration().getY());
			this.sensorInformation.getLinearAcceleration().setZ(headPose.getLinearAcceleration().getZ());
			
			this.sensorInformation.getLinearVelocity().setX(headPose.getLinearVelocity().getX());
			this.sensorInformation.getLinearVelocity().setY(headPose.getLinearVelocity().getY());
			this.sensorInformation.getLinearVelocity().setZ(headPose.getLinearVelocity().getZ());
			
			this.sensorInformation.setPoseTimeInSeconds(headPose.getTimeInSeconds());
			
			this.sensorInformation.getHeadOrientation().setW(headPose.getThePose().getOrientation().getW());
			this.sensorInformation.getHeadOrientation().setX(headPose.getThePose().getOrientation().getX());
			this.sensorInformation.getHeadOrientation().setY(headPose.getThePose().getOrientation().getY());
			this.sensorInformation.getHeadOrientation().setZ(headPose.getThePose().getOrientation().getZ());
			
			this.sensorInformation.getHeadPosition().setX(headPose.getThePose().getPosition().getX());
			this.sensorInformation.getHeadPosition().setY(headPose.getThePose().getPosition().getY());
			this.sensorInformation.getHeadPosition().setZ(headPose.getThePose().getPosition().getZ());

			ovrPosef cameraPose= state.getCameraPose();
			this.sensorInformation.getCameraOrientation().setW(cameraPose.getOrientation().getW());
			this.sensorInformation.getCameraOrientation().setX(cameraPose.getOrientation().getX());
			this.sensorInformation.getCameraOrientation().setY(cameraPose.getOrientation().getY());
			this.sensorInformation.getCameraOrientation().setZ(cameraPose.getOrientation().getZ());
			
			this.sensorInformation.getCameraPosition().setX(cameraPose.getPosition().getX());
			this.sensorInformation.getCameraPosition().setY(cameraPose.getPosition().getY());
			this.sensorInformation.getCameraPosition().setZ(cameraPose.getPosition().getZ());
			
			ovrPosef leveledCameraPose =state.getLeveledCameraPose();
			
			this.sensorInformation.getLeveledCameraOrientation().setW(leveledCameraPose.getOrientation().getW());
			this.sensorInformation.getLeveledCameraOrientation().setX(leveledCameraPose.getOrientation().getX());
			this.sensorInformation.getLeveledCameraOrientation().setY(leveledCameraPose.getOrientation().getY());
			this.sensorInformation.getLeveledCameraOrientation().setZ(leveledCameraPose.getOrientation().getZ());
			
			this.sensorInformation.getLeveledCameraPosition().setX(leveledCameraPose.getPosition().getX());
			this.sensorInformation.getLeveledCameraPosition().setX(leveledCameraPose.getPosition().getY());
			this.sensorInformation.getLeveledCameraPosition().setX(leveledCameraPose.getPosition().getZ());
			
			this.sensorInformation.setStatusFlags(state.getStatusFlags());
			
			this.sensorInformation.setPcTimeInMillis(System.currentTimeMillis());
			
			try {
				Thread.sleep(POLL_INTERVAL);
			} catch (InterruptedException e) {
			}
		}

	}

	public void shutdown() {
		this.run=Boolean.FALSE;
	}

	SensorInformation getSensorInformation() {
		return sensorInformation;
	}
	
	
	
}
