package danielmg.vr.jovr.api.data;

public class Vector2f {

	private float x;
	private float y;
	
	
	
	
	public float getX() {
		return x;
	}
	public float getY() {
		return y;
	}
	public void setX(float x) {
		this.x = x;
	}
	public void setY(float y) {
		this.y = y;
	}
	@Override
	public String toString() {
		return "Vector3f [x=" + x + ", y=" + y + "]";
	}
	
	
	
	
}
