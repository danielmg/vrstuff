package danielmg.vr.jovr.api.data;

public class Matrix4f {

	
	private float[][] matrix = new float[4][4];

	public float[][] getMatrix() {
		return matrix;
	}

	public void setMatrix(float[][] matrix) {
		this.matrix = matrix;
	}
	
	
	
}
