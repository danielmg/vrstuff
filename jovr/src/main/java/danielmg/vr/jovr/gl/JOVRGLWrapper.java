package danielmg.vr.jovr.gl;

import danielmg.vr.jovr.api.JOVRApi;
import danielmg.vr.jovr.api.data.RenderAPIType;

public class JOVRGLWrapper {

	
	private JOVRApi jovrApi;
	
	
	private JOVRGLWrapper() {}
	
	public static JOVRGLWrapper getInstance() {
		
		JOVRGLWrapper wrapper=new JOVRGLWrapper();
		
		wrapper.jovrApi=JOVRApi.getInstance(RenderAPIType.OpenGL);
		
	   
		return wrapper;
		
	}
	
	
}
