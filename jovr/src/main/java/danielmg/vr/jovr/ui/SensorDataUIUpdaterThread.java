package danielmg.vr.jovr.ui;


public class SensorDataUIUpdaterThread implements Runnable {

	private static final long UPDATE_INTERVAL=100;
	
	private JOVRDebug parent;
	SensorDataUIUpdaterThread(JOVRDebug parent) {
		this.parent=parent;
	}
	
	private boolean running=Boolean.TRUE;
	
	@Override
	public void run() {
		
		while (running) {
			parent.updateSensorDataUI();
			try {
				Thread.sleep(UPDATE_INTERVAL);
			} catch (InterruptedException e) {
			}			
		}

	}
	
	void shutdown() {
		this.running=Boolean.FALSE;
	}

}
