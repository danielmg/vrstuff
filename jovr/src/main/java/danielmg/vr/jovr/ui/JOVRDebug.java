package danielmg.vr.jovr.ui;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import com.google.gson.Gson;

import danielmg.vr.jovr.api.JOVRApi;
import danielmg.vr.jovr.api.data.HMDInfo;
import danielmg.vr.jovr.api.data.RenderAPIType;
import danielmg.vr.jovr.api.data.SensorInformation;

public class JOVRDebug {

	private static final String LOG_FILE_NAME="jovr.log";

	private PrintWriter logPW;
	protected Shell shlJovrDebugWindow;
	private boolean logToFile=Boolean.FALSE;
	private Button btnLogToFile;
	private Text txtProductName;
	private Text txtManufacturer;
	private Text txtSerialNumber;
	private Text txtFirmwareVersion;
	private Text txtDeviceType;
	private Text txtTrackingCapabilities;
	private Text txtConfigureTracking;
	private Text txtFustrumFar;
	private Text txtFustrumH;
	private Text txtFustrumNear;
	private Text txtFustrumV;
	private Text txtResolution;
	private Text txtDisplayDeviceName;

	private JOVRApi api;
	private SensorDataUIUpdaterThread sduut;
	private Text txtAccelX;
	private Text txtAccelY;
	private Text txtAccelZ;
	private Text txtGyroX;
	private Text txtGyroY;
	private Text txtGyroZ;
	private Text txtMagX;
	private Text txtMagY;
	private Text txtMagZ;
	private Text txtTemp;
	private Text txtPoseSec;
	private Text txtRawSec;
	
	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			JOVRDebug window = new JOVRDebug();
			window.open();
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static final NumberFormat nf =new DecimalFormat("0.0000");
	private Text txtAngAcX;
	private Text txtAngAcY;
	private Text txtAngAcZ;
	private Text txtAngVelX;
	private Text txtAngVelY;
	private Text txtAngVelZ;
	private Text txtLinAcX;
	private Text txtLinAcY;
	private Text txtLinAcZ;
	private Text txtLinVelX;
	private Text txtLinVelY;
	private Text txtLinVelZ;
	private Text txtHeadX;
	private Text txtHeadY;
	private Text txtHeadZ;
	private Text txtCamX;
	private Text txtCamY;
	private Text txtCamZ;
	private Text txtLCamX;
	private Text txtLCamY;
	private Text txtLCamZ;
	private Text txtStatusFlags;
	private Text txtHeadOrW;
	private Text txtHeadOrX;
	private Text txtHeadOrY;
	private Text txtHeadOrZ;
	private Text txtCamOrW;
	private Text txtCamOrX;
	private Text txtCamOrY;
	private Text txtCamOrZ;
	private Text txtLCamOrW;
	private Text txtLCamOrX;
	private Text txtLCamOrY;
	private Text txtLCamOrZ;
	
	private static final Gson gson = new Gson();
	//Receive new poller data
	void updateSensorDataUI() {
		final SensorInformation si = this.api.getSensorInformation();
		if(this.logToFile) logPW.println(gson.toJson(si));
		
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				
				//accel
				txtAccelX.setText(nf.format(si.getAccelerometer().getX()));
				txtAccelY.setText(nf.format(si.getAccelerometer().getY()));
				txtAccelZ.setText(nf.format(si.getAccelerometer().getZ()));
				//gyro
				txtGyroX.setText(nf.format(si.getGyro().getX()));
				txtGyroY.setText(nf.format(si.getGyro().getY()));
				txtGyroZ.setText(nf.format(si.getGyro().getZ()));
				//mag
				txtMagX.setText(nf.format(si.getMagnetometer().getX()));
				txtMagY.setText(nf.format(si.getMagnetometer().getY()));
				txtMagZ.setText(nf.format(si.getMagnetometer().getZ()));
				//other
				txtTemp.setText(nf.format(si.getTemperature()));
				txtPoseSec.setText(nf.format(si.getPoseTimeInSeconds()));
				txtRawSec.setText(nf.format(si.getTimeInSeconds()));
				txtStatusFlags.setText(""+si.getStatusFlags()); //don't format this
				//ang acc
				txtAngAcX.setText(nf.format(si.getAngularAcceleration().getX()));
				txtAngAcY.setText(nf.format(si.getAngularAcceleration().getY()));
				txtAngAcZ.setText(nf.format(si.getAngularAcceleration().getZ()));
				//ang vel
				txtAngVelX.setText(nf.format(si.getAngularVelocity().getX()));
				txtAngVelY.setText(nf.format(si.getAngularVelocity().getY()));
				txtAngVelZ.setText(nf.format(si.getAngularVelocity().getZ()));
				//lin acc
				txtLinAcX.setText(nf.format(si.getLinearAcceleration().getX()));
				txtLinAcY.setText(nf.format(si.getLinearAcceleration().getY()));
				txtLinAcZ.setText(nf.format(si.getLinearAcceleration().getZ()));
				//lin vel
				txtLinVelX.setText(nf.format(si.getLinearVelocity().getX()));
				txtLinVelY.setText(nf.format(si.getLinearVelocity().getY()));
				txtLinVelZ.setText(nf.format(si.getLinearVelocity().getZ()));
				//head pos
				txtHeadX.setText(nf.format(si.getHeadPosition().getX()));
				txtHeadY.setText(nf.format(si.getHeadPosition().getY()));
				txtHeadZ.setText(nf.format(si.getHeadPosition().getZ()));
				//cam pos
				txtCamX.setText(nf.format(si.getCameraPosition().getX()));
				txtCamY.setText(nf.format(si.getCameraPosition().getY()));
				txtCamZ.setText(nf.format(si.getCameraPosition().getZ()));
				//level cam pos
				txtLCamX.setText(nf.format(si.getLeveledCameraPosition().getX()));
				txtLCamY.setText(nf.format(si.getLeveledCameraPosition().getY()));
				txtLCamZ.setText(nf.format(si.getLeveledCameraPosition().getZ()));
				//head or
				txtHeadOrW.setText(nf.format(si.getHeadOrientation().getW()));
				txtHeadOrX.setText(nf.format(si.getHeadOrientation().getX()));
				txtHeadOrY.setText(nf.format(si.getHeadOrientation().getY()));
				txtHeadOrZ.setText(nf.format(si.getHeadOrientation().getZ()));
				//cam or
				txtCamOrW.setText(nf.format(si.getCameraOrientation().getW()));
				txtCamOrX.setText(nf.format(si.getCameraOrientation().getX()));
				txtCamOrY.setText(nf.format(si.getCameraOrientation().getY()));
				txtCamOrZ.setText(nf.format(si.getCameraOrientation().getZ()));
				//leveled cam or
				txtLCamOrW.setText(nf.format(si.getLeveledCameraOrientation().getW()));
				txtLCamOrX.setText(nf.format(si.getLeveledCameraOrientation().getX()));
				txtLCamOrY.setText(nf.format(si.getLeveledCameraOrientation().getY()));
				txtLCamOrZ.setText(nf.format(si.getLeveledCameraOrientation().getZ()));
		}});
	}
	
	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shlJovrDebugWindow.open();
		shlJovrDebugWindow.layout();
		
		System.out.println("Startup");
		
		this.api = JOVRApi.getInstance(RenderAPIType.OpenGL);
		
		HMDInfo hmdInfo= api.getHMDInfo();
		
		this.txtManufacturer.setText(hmdInfo.getManufacturer());
		this.txtProductName.setText(hmdInfo.getProductName());
		this.txtSerialNumber.setText(hmdInfo.getSerialNumber());
		this.txtFirmwareVersion.setText(hmdInfo.getFirmwareVersion());
		this.txtDeviceType.setText(hmdInfo.getDeviceType());
		this.txtResolution.setText(hmdInfo.getResolutionX() + " x " + hmdInfo.getResolutionY());
		this.txtDisplayDeviceName.setText(hmdInfo.getDisplayDeviceName());
		this.txtFustrumFar.setText(""+hmdInfo.getCameraFrustumFarZInMeters());
		this.txtFustrumH.setText(""+hmdInfo.getCameraFrustumHFovInRadians());
		this.txtFustrumNear.setText(""+hmdInfo.getCameraFrustumNearZInMeters());
		this.txtFustrumV.setText(""+hmdInfo.getCameraFrustumVFovInRadians());
		this.txtConfigureTracking.setText(""+api.getConfigureTracking());
		this.txtTrackingCapabilities.setText(""+hmdInfo.getTrackingCaps());
		
		//load the icon
		shlJovrDebugWindow.setImage(new Image(display, ClassLoader.getSystemResourceAsStream("Oculus.gif")));
		
		try {
			logPW= new PrintWriter(new FileOutputStream(LOG_FILE_NAME));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		
		//start updating the ui
		this.sduut=new SensorDataUIUpdaterThread(this);
		new Thread(this.sduut).start();;
		
	
		
		while (!shlJovrDebugWindow.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		this.sduut.shutdown();
		api.shutdown();
		logPW.close();
		System.out.println("Shutdown");
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlJovrDebugWindow = new Shell();
		shlJovrDebugWindow.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		shlJovrDebugWindow.setSize(1335, 559);
		shlJovrDebugWindow.setText("jOVR Debug Window");
		shlJovrDebugWindow.setLayout(null);
		
		btnLogToFile = new Button(shlJovrDebugWindow, SWT.CHECK);
		btnLogToFile.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				logToFile=!logToFile;					
			}
		});
		btnLogToFile.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		btnLogToFile.setBounds(10, 494, 211, 16);
		btnLogToFile.setText("Log Sensor Data to File: "+LOG_FILE_NAME);
		
		
		Group grpHeadOrientation = new Group(shlJovrDebugWindow, SWT.NONE);
		grpHeadOrientation.setText("Head Orientation");
		grpHeadOrientation.setForeground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		grpHeadOrientation.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.NORMAL));
		grpHeadOrientation.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		grpHeadOrientation.setBounds(1096, 13, 211, 144);
		
		Label lblWAxis = new Label(grpHeadOrientation, SWT.NONE);
		lblWAxis.setText("W Axis");
		lblWAxis.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblWAxis.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblWAxis.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		lblWAxis.setBounds(10, 18, 92, 15);
		
		txtHeadOrW = new Text(grpHeadOrientation, SWT.BORDER);
		txtHeadOrW.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtHeadOrW.setEditable(false);
		txtHeadOrW.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtHeadOrW.setBounds(108, 15, 85, 21);
		
		Label lblXAxis_1 = new Label(grpHeadOrientation, SWT.NONE);
		lblXAxis_1.setText("X Axis");
		lblXAxis_1.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblXAxis_1.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblXAxis_1.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		lblXAxis_1.setBounds(10, 51, 92, 15);
		
		txtHeadOrX = new Text(grpHeadOrientation, SWT.BORDER);
		txtHeadOrX.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtHeadOrX.setEditable(false);
		txtHeadOrX.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtHeadOrX.setBounds(108, 48, 85, 21);
		
		Label lblYAxis_1 = new Label(grpHeadOrientation, SWT.NONE);
		lblYAxis_1.setText("Y Axis");
		lblYAxis_1.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblYAxis_1.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblYAxis_1.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		lblYAxis_1.setBounds(10, 84, 92, 15);
		
		txtHeadOrY = new Text(grpHeadOrientation, SWT.BORDER);
		txtHeadOrY.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtHeadOrY.setEditable(false);
		txtHeadOrY.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtHeadOrY.setBounds(108, 81, 85, 21);
		
		Label lblZAxis_1 = new Label(grpHeadOrientation, SWT.NONE);
		lblZAxis_1.setText("Z Axis");
		lblZAxis_1.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblZAxis_1.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblZAxis_1.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		lblZAxis_1.setBounds(10, 116, 92, 15);
		
		txtHeadOrZ = new Text(grpHeadOrientation, SWT.BORDER);
		txtHeadOrZ.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtHeadOrZ.setEditable(false);
		txtHeadOrZ.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtHeadOrZ.setBounds(108, 113, 85, 21);
		
		Group grpCameraOrientation = new Group(shlJovrDebugWindow, SWT.NONE);
		grpCameraOrientation.setText("Camera Orientation");
		grpCameraOrientation.setForeground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		grpCameraOrientation.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.NORMAL));
		grpCameraOrientation.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		grpCameraOrientation.setBounds(1096, 165, 211, 144);
		
		Label labele = new Label(grpCameraOrientation, SWT.NONE);
		labele.setText("W Axis");
		labele.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		labele.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		labele.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		labele.setBounds(10, 18, 92, 15);
		
		txtCamOrW = new Text(grpCameraOrientation, SWT.BORDER);
		txtCamOrW.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtCamOrW.setEditable(false);
		txtCamOrW.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtCamOrW.setBounds(108, 15, 85, 21);
		
		Label label_1e = new Label(grpCameraOrientation, SWT.NONE);
		label_1e.setText("X Axis");
		label_1e.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		label_1e.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		label_1e.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		label_1e.setBounds(10, 51, 92, 15);
		
		txtCamOrX = new Text(grpCameraOrientation, SWT.BORDER);
		txtCamOrX.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtCamOrX.setEditable(false);
		txtCamOrX.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtCamOrX.setBounds(108, 48, 85, 21);
		
		Label label_2e = new Label(grpCameraOrientation, SWT.NONE);
		label_2e.setText("Y Axis");
		label_2e.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		label_2e.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		label_2e.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		label_2e.setBounds(10, 84, 92, 15);
		
		txtCamOrY = new Text(grpCameraOrientation, SWT.BORDER);
		txtCamOrY.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtCamOrY.setEditable(false);
		txtCamOrY.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtCamOrY.setBounds(108, 81, 85, 21);
		
		Label label_3e = new Label(grpCameraOrientation, SWT.NONE);
		label_3e.setText("Z Axis");
		label_3e.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		label_3e.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		label_3e.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		label_3e.setBounds(10, 116, 92, 15);
		
		txtCamOrZ = new Text(grpCameraOrientation, SWT.BORDER);
		txtCamOrZ.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtCamOrZ.setEditable(false);
		txtCamOrZ.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtCamOrZ.setBounds(108, 113, 85, 21);
		
		Group grpLeveledCameraOrientation = new Group(shlJovrDebugWindow, SWT.NONE);
		grpLeveledCameraOrientation.setText("Leveled Camera Orientation");
		grpLeveledCameraOrientation.setForeground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		grpLeveledCameraOrientation.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.NORMAL));
		grpLeveledCameraOrientation.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		grpLeveledCameraOrientation.setBounds(1096, 315, 211, 144);
		
		Label label_4f = new Label(grpLeveledCameraOrientation, SWT.NONE);
		label_4f.setText("W Axis");
		label_4f.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		label_4f.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		label_4f.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		label_4f.setBounds(10, 18, 92, 15);
		
		txtLCamOrW = new Text(grpLeveledCameraOrientation, SWT.BORDER);
		txtLCamOrW.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtLCamOrW.setEditable(false);
		txtLCamOrW.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtLCamOrW.setBounds(108, 15, 85, 21);
		
		Label label_5f = new Label(grpLeveledCameraOrientation, SWT.NONE);
		label_5f.setText("X Axis");
		label_5f.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		label_5f.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		label_5f.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		label_5f.setBounds(10, 51, 92, 15);
		
		txtLCamOrX = new Text(grpLeveledCameraOrientation, SWT.BORDER);
		txtLCamOrX.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtLCamOrX.setEditable(false);
		txtLCamOrX.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtLCamOrX.setBounds(108, 48, 85, 21);
		
		Label label_6f = new Label(grpLeveledCameraOrientation, SWT.NONE);
		label_6f.setText("Y Axis");
		label_6f.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		label_6f.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		label_6f.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		label_6f.setBounds(10, 84, 92, 15);
		
		txtLCamOrY = new Text(grpLeveledCameraOrientation, SWT.BORDER);
		txtLCamOrY.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtLCamOrY.setEditable(false);
		txtLCamOrY.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtLCamOrY.setBounds(108, 81, 85, 21);
		
		Label label_7f = new Label(grpLeveledCameraOrientation, SWT.NONE);
		label_7f.setText("Z Axis");
		label_7f.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		label_7f.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		label_7f.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		label_7f.setBounds(10, 116, 92, 15);
		
		txtLCamOrZ = new Text(grpLeveledCameraOrientation, SWT.BORDER);
		txtLCamOrZ.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtLCamOrZ.setEditable(false);
		txtLCamOrZ.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtLCamOrZ.setBounds(108, 113, 85, 21);
	
		
		Group grpHead = new Group(shlJovrDebugWindow, SWT.NONE);
		grpHead.setText("Head Position");
		grpHead.setForeground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		grpHead.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.NORMAL));
		grpHead.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		grpHead.setBounds(879, 13, 211, 111);
		
		Label labela = new Label(grpHead, SWT.NONE);
		labela.setText("X Axis");
		labela.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		labela.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		labela.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		labela.setBounds(10, 18, 92, 15);
		
		txtHeadX = new Text(grpHead, SWT.BORDER);
		txtHeadX.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtHeadX.setEditable(false);
		txtHeadX.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtHeadX.setBounds(108, 15, 85, 21);
		
		Label label_1a = new Label(grpHead, SWT.NONE);
		label_1a.setText("Y Axis");
		label_1a.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		label_1a.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		label_1a.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		label_1a.setBounds(10, 51, 92, 15);
		
		txtHeadY = new Text(grpHead, SWT.BORDER);
		txtHeadY.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtHeadY.setEditable(false);
		txtHeadY.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtHeadY.setBounds(108, 48, 85, 21);
		
		Label label_2a = new Label(grpHead, SWT.NONE);
		label_2a.setText("Z Axis");
		label_2a.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		label_2a.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		label_2a.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		label_2a.setBounds(10, 84, 92, 15);
		
		txtHeadZ = new Text(grpHead, SWT.BORDER);
		txtHeadZ.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtHeadZ.setEditable(false);
		txtHeadZ.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtHeadZ.setBounds(108, 81, 85, 21);
		
		Group grpCameraPosition = new Group(shlJovrDebugWindow, SWT.NONE);
		grpCameraPosition.setText("Camera Position");
		grpCameraPosition.setForeground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		grpCameraPosition.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.NORMAL));
		grpCameraPosition.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		grpCameraPosition.setBounds(879, 132, 211, 111);
		
		Label labelb = new Label(grpCameraPosition, SWT.NONE);
		labelb.setText("X Axis");
		labelb.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		labelb.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		labelb.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		labelb.setBounds(10, 18, 92, 15);
		
		txtCamX = new Text(grpCameraPosition, SWT.BORDER);
		txtCamX.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtCamX.setEditable(false);
		txtCamX.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtCamX.setBounds(108, 15, 85, 21);
		
		Label label_1b = new Label(grpCameraPosition, SWT.NONE);
		label_1b.setText("Y Axis");
		label_1b.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		label_1b.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		label_1b.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		label_1b.setBounds(10, 51, 92, 15);
		
		txtCamY = new Text(grpCameraPosition, SWT.BORDER);
		txtCamY.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtCamY.setEditable(false);
		txtCamY.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtCamY.setBounds(108, 48, 85, 21);
		
		Label label_2c = new Label(grpCameraPosition, SWT.NONE);
		label_2c.setText("Z Axis");
		label_2c.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		label_2c.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		label_2c.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		label_2c.setBounds(10, 84, 92, 15);
		
		txtCamZ = new Text(grpCameraPosition, SWT.BORDER);
		txtCamZ.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtCamZ.setEditable(false);
		txtCamZ.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtCamZ.setBounds(108, 81, 85, 21);
		
		Group grpLeveledCameraPosition = new Group(shlJovrDebugWindow, SWT.NONE);
		grpLeveledCameraPosition.setText("Leveled Camera Position");
		grpLeveledCameraPosition.setForeground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		grpLeveledCameraPosition.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.NORMAL));
		grpLeveledCameraPosition.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		grpLeveledCameraPosition.setBounds(879, 247, 211, 111);
		
		Label labeld = new Label(grpLeveledCameraPosition, SWT.NONE);
		labeld.setText("X Axis");
		labeld.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		labeld.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		labeld.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		labeld.setBounds(10, 18, 92, 15);
		
		txtLCamX = new Text(grpLeveledCameraPosition, SWT.BORDER);
		txtLCamX.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtLCamX.setEditable(false);
		txtLCamX.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtLCamX.setBounds(108, 15, 85, 21);
		
		Label label_1d = new Label(grpLeveledCameraPosition, SWT.NONE);
		label_1d.setText("Y Axis");
		label_1d.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		label_1d.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		label_1d.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		label_1d.setBounds(10, 51, 92, 15);
		
		txtLCamY = new Text(grpLeveledCameraPosition, SWT.BORDER);
		txtLCamY.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtLCamY.setEditable(false);
		txtLCamY.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtLCamY.setBounds(108, 48, 85, 21);
		
		Label label_2d = new Label(grpLeveledCameraPosition, SWT.NONE);
		label_2d.setText("Z Axis");
		label_2d.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		label_2d.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		label_2d.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		label_2d.setBounds(10, 84, 92, 15);
		
		txtLCamZ = new Text(grpLeveledCameraPosition, SWT.BORDER);
		txtLCamZ.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtLCamZ.setEditable(false);
		txtLCamZ.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtLCamZ.setBounds(108, 81, 85, 21);
		
		
		Group grpLinearAcceleration = new Group(shlJovrDebugWindow, SWT.NONE);
		grpLinearAcceleration.setText("Linear Acceleration");
		grpLinearAcceleration.setForeground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		grpLinearAcceleration.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.NORMAL));
		grpLinearAcceleration.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		grpLinearAcceleration.setBounds(662, 247, 211, 111);
		
		Label labelz = new Label(grpLinearAcceleration, SWT.NONE);
		labelz.setText("X Axis");
		labelz.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		labelz.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		labelz.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		labelz.setBounds(10, 18, 92, 15);
		
		txtLinAcX = new Text(grpLinearAcceleration, SWT.BORDER);
		txtLinAcX.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtLinAcX.setEditable(false);
		txtLinAcX.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtLinAcX.setBounds(108, 15, 85, 21);
		
		Label label_1z = new Label(grpLinearAcceleration, SWT.NONE);
		label_1z.setText("Y Axis");
		label_1z.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		label_1z.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		label_1z.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		label_1z.setBounds(10, 51, 92, 15);
		
		txtLinAcY = new Text(grpLinearAcceleration, SWT.BORDER);
		txtLinAcY.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtLinAcY.setEditable(false);
		txtLinAcY.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtLinAcY.setBounds(108, 48, 85, 21);
		
		Label label_2z = new Label(grpLinearAcceleration, SWT.NONE);
		label_2z.setText("Z Axis");
		label_2z.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		label_2z.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		label_2z.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		label_2z.setBounds(10, 84, 92, 15);
		
		txtLinAcZ = new Text(grpLinearAcceleration, SWT.BORDER);
		txtLinAcZ.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtLinAcZ.setEditable(false);
		txtLinAcZ.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtLinAcZ.setBounds(108, 81, 85, 21);
		
		Group grpLinearVelocity = new Group(shlJovrDebugWindow, SWT.NONE);
		grpLinearVelocity.setText("Linear Velocity");
		grpLinearVelocity.setForeground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		grpLinearVelocity.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.NORMAL));
		grpLinearVelocity.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		grpLinearVelocity.setBounds(662, 366, 211, 111);
		
		Label label_3x = new Label(grpLinearVelocity, SWT.NONE);
		label_3x.setText("X Axis");
		label_3x.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		label_3x.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		label_3x.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		label_3x.setBounds(10, 18, 92, 15);
		
		txtLinVelX = new Text(grpLinearVelocity, SWT.BORDER);
		txtLinVelX.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtLinVelX.setEditable(false);
		txtLinVelX.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtLinVelX.setBounds(108, 15, 85, 21);
		
		Label label_4x = new Label(grpLinearVelocity, SWT.NONE);
		label_4x.setText("Y Axis");
		label_4x.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		label_4x.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		label_4x.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		label_4x.setBounds(10, 51, 92, 15);
		
		txtLinVelY = new Text(grpLinearVelocity, SWT.BORDER);
		txtLinVelY.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtLinVelY.setEditable(false);
		txtLinVelY.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtLinVelY.setBounds(108, 48, 85, 21);
		
		Label label_5x = new Label(grpLinearVelocity, SWT.NONE);
		label_5x.setText("Z Axis");
		label_5x.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		label_5x.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		label_5x.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		label_5x.setBounds(10, 84, 92, 15);
		
		txtLinVelZ = new Text(grpLinearVelocity, SWT.BORDER);
		txtLinVelZ.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtLinVelZ.setEditable(false);
		txtLinVelZ.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtLinVelZ.setBounds(108, 81, 85, 21);
		
		Group grpAngularAcceleration = new Group(shlJovrDebugWindow, SWT.NONE);
		grpAngularAcceleration.setText("Angular Acceleration");
		grpAngularAcceleration.setForeground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		grpAngularAcceleration.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.NORMAL));
		grpAngularAcceleration.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		grpAngularAcceleration.setBounds(662, 13, 211, 111);
		
		Label labely = new Label(grpAngularAcceleration, SWT.NONE);
		labely.setText("X Axis");
		labely.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		labely.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		labely.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		labely.setBounds(10, 18, 92, 15);
		
		txtAngAcX = new Text(grpAngularAcceleration, SWT.BORDER);
		txtAngAcX.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtAngAcX.setEditable(false);
		txtAngAcX.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtAngAcX.setBounds(108, 15, 85, 21);
		
		Label label_1y = new Label(grpAngularAcceleration, SWT.NONE);
		label_1y.setText("Y Axis");
		label_1y.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		label_1y.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		label_1y.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		label_1y.setBounds(10, 51, 92, 15);
		
		txtAngAcY = new Text(grpAngularAcceleration, SWT.BORDER);
		txtAngAcY.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtAngAcY.setEditable(false);
		txtAngAcY.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtAngAcY.setBounds(108, 48, 85, 21);
		
		Label label_2y = new Label(grpAngularAcceleration, SWT.NONE);
		label_2y.setText("Z Axis");
		label_2y.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		label_2y.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		label_2y.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		label_2y.setBounds(10, 84, 92, 15);
		
		txtAngAcZ = new Text(grpAngularAcceleration, SWT.BORDER);
		txtAngAcZ.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtAngAcZ.setEditable(false);
		txtAngAcZ.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtAngAcZ.setBounds(108, 81, 85, 21);
		
		Group grpAngularVelocity = new Group(shlJovrDebugWindow, SWT.NONE);
		grpAngularVelocity.setText("Angular Velocity");
		grpAngularVelocity.setForeground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		grpAngularVelocity.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.NORMAL));
		grpAngularVelocity.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		grpAngularVelocity.setBounds(662, 132, 211, 111);
		
		Label label_3 = new Label(grpAngularVelocity, SWT.NONE);
		label_3.setText("X Axis");
		label_3.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		label_3.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		label_3.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		label_3.setBounds(10, 18, 92, 15);
		
		txtAngVelX = new Text(grpAngularVelocity, SWT.BORDER);
		txtAngVelX.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtAngVelX.setEditable(false);
		txtAngVelX.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtAngVelX.setBounds(108, 15, 85, 21);
		
		Label label_4 = new Label(grpAngularVelocity, SWT.NONE);
		label_4.setText("Y Axis");
		label_4.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		label_4.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		label_4.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		label_4.setBounds(10, 51, 92, 15);
		
		txtAngVelY = new Text(grpAngularVelocity, SWT.BORDER);
		txtAngVelY.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtAngVelY.setEditable(false);
		txtAngVelY.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtAngVelY.setBounds(108, 48, 85, 21);
		
		Label label_5 = new Label(grpAngularVelocity, SWT.NONE);
		label_5.setText("Z Axis");
		label_5.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		label_5.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		label_5.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		label_5.setBounds(10, 84, 92, 15);
		
		txtAngVelZ = new Text(grpAngularVelocity, SWT.BORDER);
		txtAngVelZ.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtAngVelZ.setEditable(false);
		txtAngVelZ.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtAngVelZ.setBounds(108, 81, 85, 21);
		
		
		Group grpOtherInfo = new Group(shlJovrDebugWindow, SWT.NONE);
		grpOtherInfo.setText("Other Info");
		grpOtherInfo.setForeground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		grpOtherInfo.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.NORMAL));
		grpOtherInfo.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		grpOtherInfo.setBounds(879, 366, 211, 144);
		
		Label lblTemperature = new Label(grpOtherInfo, SWT.NONE);
		lblTemperature.setText("Temperature");
		lblTemperature.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblTemperature.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblTemperature.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		lblTemperature.setBounds(10, 18, 92, 15);
		
		txtTemp = new Text(grpOtherInfo, SWT.BORDER);
		txtTemp.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtTemp.setEditable(false);
		txtTemp.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtTemp.setBounds(108, 15, 85, 21);
		
		Label lblPoseSeconds = new Label(grpOtherInfo, SWT.NONE);
		lblPoseSeconds.setText("Pose Seconds");
		lblPoseSeconds.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblPoseSeconds.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblPoseSeconds.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		lblPoseSeconds.setBounds(10, 51, 92, 15);
		
		txtPoseSec = new Text(grpOtherInfo, SWT.BORDER);
		txtPoseSec.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtPoseSec.setEditable(false);
		txtPoseSec.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtPoseSec.setBounds(108, 48, 85, 21);
		
		Label lblRawSeconds = new Label(grpOtherInfo, SWT.NONE);
		lblRawSeconds.setText("Raw Seconds");
		lblRawSeconds.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblRawSeconds.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblRawSeconds.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		lblRawSeconds.setBounds(10, 84, 92, 15);
		
		txtRawSec = new Text(grpOtherInfo, SWT.BORDER);
		txtRawSec.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtRawSec.setEditable(false);
		txtRawSec.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtRawSec.setBounds(108, 81, 85, 21);
		
		Label lblStatusFlags = new Label(grpOtherInfo, SWT.NONE);
		lblStatusFlags.setText("Status Flags");
		lblStatusFlags.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblStatusFlags.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblStatusFlags.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		lblStatusFlags.setBounds(10, 116, 92, 15);
		
		txtStatusFlags = new Text(grpOtherInfo, SWT.BORDER);
		txtStatusFlags.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtStatusFlags.setEditable(false);
		txtStatusFlags.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtStatusFlags.setBounds(108, 113, 85, 21);
		
		Group grpMagnometer = new Group(shlJovrDebugWindow, SWT.NONE);
		grpMagnometer.setText("Magnometer");
		grpMagnometer.setForeground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		grpMagnometer.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.NORMAL));
		grpMagnometer.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		grpMagnometer.setBounds(440, 249, 211, 111);
		
		Label label = new Label(grpMagnometer, SWT.NONE);
		label.setText("X Axis");
		label.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		label.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		label.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		label.setBounds(10, 18, 92, 15);
		
		txtMagX = new Text(grpMagnometer, SWT.BORDER);
		txtMagX.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtMagX.setEditable(false);
		txtMagX.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtMagX.setBounds(108, 15, 85, 21);
		
		Label label_1 = new Label(grpMagnometer, SWT.NONE);
		label_1.setText("Y Axis");
		label_1.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		label_1.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		label_1.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		label_1.setBounds(10, 51, 92, 15);
		
		txtMagY = new Text(grpMagnometer, SWT.BORDER);
		txtMagY.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtMagY.setEditable(false);
		txtMagY.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtMagY.setBounds(108, 48, 85, 21);
		
		Label label_2 = new Label(grpMagnometer, SWT.NONE);
		label_2.setText("Z Axis");
		label_2.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		label_2.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		label_2.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		label_2.setBounds(10, 84, 92, 15);
		
		txtMagZ = new Text(grpMagnometer, SWT.BORDER);
		txtMagZ.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtMagZ.setEditable(false);
		txtMagZ.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtMagZ.setBounds(108, 81, 85, 21);
		Group grpAccelerometer = new Group(shlJovrDebugWindow, SWT.NONE);
		grpAccelerometer.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		grpAccelerometer.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.NORMAL));
		grpAccelerometer.setForeground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		grpAccelerometer.setText("Accelerometer");
		grpAccelerometer.setBounds(440, 13, 211, 111);
		
		Label lblXAxis = new Label(grpAccelerometer, SWT.NONE);
		lblXAxis.setText("X Axis");
		lblXAxis.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblXAxis.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblXAxis.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		lblXAxis.setBounds(10, 18, 92, 15);
		
		txtAccelX = new Text(grpAccelerometer, SWT.BORDER);
		txtAccelX.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtAccelX.setEditable(false);
		txtAccelX.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtAccelX.setBounds(108, 15, 85, 21);
		
		Label lblYAxis = new Label(grpAccelerometer, SWT.NONE);
		lblYAxis.setText("Y Axis");
		lblYAxis.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblYAxis.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblYAxis.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		lblYAxis.setBounds(10, 51, 92, 15);
		
		txtAccelY = new Text(grpAccelerometer, SWT.BORDER);
		txtAccelY.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtAccelY.setEditable(false);
		txtAccelY.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtAccelY.setBounds(108, 48, 85, 21);
		
		Label lblZAxis = new Label(grpAccelerometer, SWT.NONE);
		lblZAxis.setText("Z Axis");
		lblZAxis.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblZAxis.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblZAxis.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		lblZAxis.setBounds(10, 84, 92, 15);
		
		txtAccelZ = new Text(grpAccelerometer, SWT.BORDER);
		txtAccelZ.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtAccelZ.setEditable(false);
		txtAccelZ.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtAccelZ.setBounds(108, 81, 85, 21);
		
		Group grpGyro = new Group(shlJovrDebugWindow, SWT.NONE);
		grpGyro.setText("Gyro");
		grpGyro.setForeground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		grpGyro.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.NORMAL));
		grpGyro.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		grpGyro.setBounds(440, 132, 211, 111);
		
		Label labelx = new Label(grpGyro, SWT.NONE);
		labelx.setText("X Axis");
		labelx.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		labelx.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		labelx.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		labelx.setBounds(10, 18, 92, 15);
		
		txtGyroX = new Text(grpGyro, SWT.BORDER);
		txtGyroX.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtGyroX.setEditable(false);
		txtGyroX.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtGyroX.setBounds(108, 15, 85, 21);
		
		Label label_1x = new Label(grpGyro, SWT.NONE);
		label_1x.setText("Y Axis");
		label_1x.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		label_1x.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		label_1x.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		label_1x.setBounds(10, 51, 92, 15);
		
		txtGyroY = new Text(grpGyro, SWT.BORDER);
		txtGyroY.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtGyroY.setEditable(false);
		txtGyroY.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtGyroY.setBounds(108, 48, 85, 21);
		
		Label label_2x = new Label(grpGyro, SWT.NONE);
		label_2x.setText("Z Axis");
		label_2x.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		label_2x.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		label_2x.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		label_2x.setBounds(10, 84, 92, 15);
		
		txtGyroZ = new Text(grpGyro, SWT.BORDER);
		txtGyroZ.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtGyroZ.setEditable(false);
		txtGyroZ.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtGyroZ.setBounds(108, 81, 85, 21);
		
		Label lblNewLabel = new Label(shlJovrDebugWindow, SWT.NONE);
		lblNewLabel.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		lblNewLabel.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblNewLabel.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblNewLabel.setBounds(10, 21, 211, 15);
		lblNewLabel.setText("Manufacturer");
		
		Label lblNewLabel_1 = new Label(shlJovrDebugWindow, SWT.NONE);
		lblNewLabel_1.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		lblNewLabel_1.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblNewLabel_1.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblNewLabel_1.setBounds(10, 57, 211, 15);
		lblNewLabel_1.setText("Product name");
		
		Label lblNewLabel_2 = new Label(shlJovrDebugWindow, SWT.NONE);
		lblNewLabel_2.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		lblNewLabel_2.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblNewLabel_2.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblNewLabel_2.setBounds(10, 93, 211, 15);
		lblNewLabel_2.setText("Serial number");
		
		Label lblNewLabel_3 = new Label(shlJovrDebugWindow, SWT.NONE);
		lblNewLabel_3.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		lblNewLabel_3.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblNewLabel_3.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblNewLabel_3.setBounds(10, 129, 211, 15);
		lblNewLabel_3.setText("Firmware version");
		
		Label lblNewLabel_4 = new Label(shlJovrDebugWindow, SWT.NONE);
		lblNewLabel_4.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		lblNewLabel_4.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblNewLabel_4.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblNewLabel_4.setBounds(10, 165, 211, 15);
		lblNewLabel_4.setText("Device Type");
		
		Label lblNewLabel_5 = new Label(shlJovrDebugWindow, SWT.NONE);
		lblNewLabel_5.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		lblNewLabel_5.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblNewLabel_5.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblNewLabel_5.setBounds(10, 201, 211, 15);
		lblNewLabel_5.setText("Tracking capabilities");
		
		Label lblNewLabel_6 = new Label(shlJovrDebugWindow, SWT.NONE);
		lblNewLabel_6.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		lblNewLabel_6.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblNewLabel_6.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblNewLabel_6.setBounds(10, 237, 211, 15);
		lblNewLabel_6.setText("Configure tracking returned");
		
		Label lblNewLabel_7 = new Label(shlJovrDebugWindow, SWT.NONE);
		lblNewLabel_7.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		lblNewLabel_7.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblNewLabel_7.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblNewLabel_7.setBounds(10, 273, 211, 15);
		lblNewLabel_7.setText("Camera Frustum Far Z In Meters");
		
		Label lblNewLabel_8 = new Label(shlJovrDebugWindow, SWT.NONE);
		lblNewLabel_8.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		lblNewLabel_8.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblNewLabel_8.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblNewLabel_8.setBounds(10, 309, 211, 15);
		lblNewLabel_8.setText("Camera Frustum H Fov In Radians");
		
		Label lblNewLabel_9 = new Label(shlJovrDebugWindow, SWT.NONE);
		lblNewLabel_9.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		lblNewLabel_9.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblNewLabel_9.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblNewLabel_9.setBounds(10, 345, 211, 15);
		lblNewLabel_9.setText("Camera Frustum Near Z In Meters");
		
		Label lblNewLabel_10 = new Label(shlJovrDebugWindow, SWT.NONE);
		lblNewLabel_10.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		lblNewLabel_10.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblNewLabel_10.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblNewLabel_10.setBounds(10, 381, 211, 15);
		lblNewLabel_10.setText("Camera Frustum V Fov In Radians");
		
		Label lblResolution = new Label(shlJovrDebugWindow, SWT.NONE);
		lblResolution.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		lblResolution.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblResolution.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblResolution.setBounds(10, 417, 211, 15);
		lblResolution.setText("Resolution");
		
		Label lblDisplayDeviceName = new Label(shlJovrDebugWindow, SWT.NONE);
		lblDisplayDeviceName.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		lblDisplayDeviceName.setForeground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		lblDisplayDeviceName.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblDisplayDeviceName.setBounds(10, 453, 211, 15);
		lblDisplayDeviceName.setText("Display Device Name");
		
		txtProductName = new Text(shlJovrDebugWindow, SWT.BORDER);
		txtProductName.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtProductName.setEditable(false);
		txtProductName.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtProductName.setBounds(227, 54, 184, 21);
		
		txtManufacturer = new Text(shlJovrDebugWindow, SWT.BORDER);
		txtManufacturer.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtManufacturer.setEditable(false);
		txtManufacturer.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtManufacturer.setBounds(227, 18, 184, 21);
		
		txtSerialNumber = new Text(shlJovrDebugWindow, SWT.BORDER);
		txtSerialNumber.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtSerialNumber.setEditable(false);
		txtSerialNumber.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtSerialNumber.setBounds(227, 90, 184, 21);
		
		txtFirmwareVersion = new Text(shlJovrDebugWindow, SWT.BORDER);
		txtFirmwareVersion.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtFirmwareVersion.setEditable(false);
		txtFirmwareVersion.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtFirmwareVersion.setBounds(227, 126, 184, 21);
		
		txtDeviceType = new Text(shlJovrDebugWindow, SWT.BORDER);
		txtDeviceType.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtDeviceType.setEditable(false);
		txtDeviceType.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtDeviceType.setBounds(227, 162, 184, 21);
		
		txtTrackingCapabilities = new Text(shlJovrDebugWindow, SWT.BORDER);
		txtTrackingCapabilities.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtTrackingCapabilities.setEditable(false);
		txtTrackingCapabilities.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtTrackingCapabilities.setBounds(227, 198, 184, 21);
		
		txtConfigureTracking = new Text(shlJovrDebugWindow, SWT.BORDER);
		txtConfigureTracking.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtConfigureTracking.setEditable(false);
		txtConfigureTracking.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtConfigureTracking.setBounds(227, 234, 184, 21);
		
		txtFustrumFar = new Text(shlJovrDebugWindow, SWT.BORDER);
		txtFustrumFar.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtFustrumFar.setEditable(false);
		txtFustrumFar.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtFustrumFar.setBounds(227, 270, 184, 21);
		
		txtFustrumH = new Text(shlJovrDebugWindow, SWT.BORDER);
		txtFustrumH.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtFustrumH.setEditable(false);
		txtFustrumH.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtFustrumH.setBounds(227, 306, 184, 21);
		
		txtFustrumNear = new Text(shlJovrDebugWindow, SWT.BORDER);
		txtFustrumNear.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtFustrumNear.setEditable(false);
		txtFustrumNear.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtFustrumNear.setBounds(227, 342, 184, 21);
		
		txtFustrumV = new Text(shlJovrDebugWindow, SWT.BORDER);
		txtFustrumV.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtFustrumV.setEditable(false);
		txtFustrumV.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtFustrumV.setBounds(227, 378, 184, 21);
		
		txtResolution = new Text(shlJovrDebugWindow, SWT.BORDER);
		txtResolution.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtResolution.setEditable(false);
		txtResolution.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtResolution.setBounds(227, 414, 184, 21);
		
		txtDisplayDeviceName = new Text(shlJovrDebugWindow, SWT.BORDER);
		txtDisplayDeviceName.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtDisplayDeviceName.setEditable(false);
		txtDisplayDeviceName.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
		txtDisplayDeviceName.setBounds(227, 450, 184, 21);

	}
	
	
}
