/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 3.0.2
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package danielmg.vr.jovr.jni;

public class ovrDistortionVertex {
  private long swigCPtr;
  protected boolean swigCMemOwn;

  protected ovrDistortionVertex(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(ovrDistortionVertex obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        OvrCapiJNI.delete_ovrDistortionVertex(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public void setScreenPosNDC(ovrVector2f value) {
    OvrCapiJNI.ovrDistortionVertex_ScreenPosNDC_set(swigCPtr, this, ovrVector2f.getCPtr(value), value);
  }

  public ovrVector2f getScreenPosNDC() {
    long cPtr = OvrCapiJNI.ovrDistortionVertex_ScreenPosNDC_get(swigCPtr, this);
    return (cPtr == 0) ? null : new ovrVector2f(cPtr, false);
  }

  public void setTimeWarpFactor(float value) {
    OvrCapiJNI.ovrDistortionVertex_TimeWarpFactor_set(swigCPtr, this, value);
  }

  public float getTimeWarpFactor() {
    return OvrCapiJNI.ovrDistortionVertex_TimeWarpFactor_get(swigCPtr, this);
  }

  public void setVignetteFactor(float value) {
    OvrCapiJNI.ovrDistortionVertex_VignetteFactor_set(swigCPtr, this, value);
  }

  public float getVignetteFactor() {
    return OvrCapiJNI.ovrDistortionVertex_VignetteFactor_get(swigCPtr, this);
  }

  public void setTanEyeAnglesR(ovrVector2f value) {
    OvrCapiJNI.ovrDistortionVertex_TanEyeAnglesR_set(swigCPtr, this, ovrVector2f.getCPtr(value), value);
  }

  public ovrVector2f getTanEyeAnglesR() {
    long cPtr = OvrCapiJNI.ovrDistortionVertex_TanEyeAnglesR_get(swigCPtr, this);
    return (cPtr == 0) ? null : new ovrVector2f(cPtr, false);
  }

  public void setTanEyeAnglesG(ovrVector2f value) {
    OvrCapiJNI.ovrDistortionVertex_TanEyeAnglesG_set(swigCPtr, this, ovrVector2f.getCPtr(value), value);
  }

  public ovrVector2f getTanEyeAnglesG() {
    long cPtr = OvrCapiJNI.ovrDistortionVertex_TanEyeAnglesG_get(swigCPtr, this);
    return (cPtr == 0) ? null : new ovrVector2f(cPtr, false);
  }

  public void setTanEyeAnglesB(ovrVector2f value) {
    OvrCapiJNI.ovrDistortionVertex_TanEyeAnglesB_set(swigCPtr, this, ovrVector2f.getCPtr(value), value);
  }

  public ovrVector2f getTanEyeAnglesB() {
    long cPtr = OvrCapiJNI.ovrDistortionVertex_TanEyeAnglesB_get(swigCPtr, this);
    return (cPtr == 0) ? null : new ovrVector2f(cPtr, false);
  }

  public ovrDistortionVertex() {
    this(OvrCapiJNI.new_ovrDistortionVertex(), true);
  }

}
