/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 3.0.2
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package danielmg.vr.jovr.jni;

public class ovrPosef {
	private long swigCPtr;
	protected boolean swigCMemOwn;

	protected ovrPosef(long cPtr, boolean cMemoryOwn) {
		swigCMemOwn = cMemoryOwn;
		swigCPtr = cPtr;
	}

	protected static long getCPtr(ovrPosef obj) {
		return (obj == null) ? 0 : obj.swigCPtr;
	}

	protected void finalize() {
		delete();
	}

	public synchronized void delete() {
		if (swigCPtr != 0) {
			if (swigCMemOwn) {
				swigCMemOwn = false;
				OvrCapiJNI.delete_ovrPosef(swigCPtr);
			}
			swigCPtr = 0;
		}
	}

	public void setOrientation(ovrQuatf value) {
		OvrCapiJNI.ovrPosef_Orientation_set(swigCPtr, this,
				ovrQuatf.getCPtr(value), value);
	}

	public ovrQuatf getOrientation() {
		long cPtr = OvrCapiJNI.ovrPosef_Orientation_get(swigCPtr, this);
		return (cPtr == 0) ? null : new ovrQuatf(cPtr, false);
	}

	public void setPosition(ovrVector3f value) {
		OvrCapiJNI.ovrPosef_Position_set(swigCPtr, this,
				ovrVector3f.getCPtr(value), value);
	}

	public ovrVector3f getPosition() {
		long cPtr = OvrCapiJNI.ovrPosef_Position_get(swigCPtr, this);
		return (cPtr == 0) ? null : new ovrVector3f(cPtr, false);
	}

	public ovrPosef() {
		this(OvrCapiJNI.new_ovrPosef(), true);
	}

	protected static long[] cArrayUnwrap(ovrPosef[] arrayWrapper) {
		long[] cArray = new long[arrayWrapper.length];
		for (int i = 0; i < arrayWrapper.length; i++)
			cArray[i] = ovrPosef.getCPtr(arrayWrapper[i]);
		return cArray;
	}

	protected static ovrPosef[] cArrayWrap(long[] cArray, boolean cMemoryOwn) {
		ovrPosef[] arrayWrapper = new ovrPosef[cArray.length];
		for (int i = 0; i < cArray.length; i++)
			arrayWrapper[i] = new ovrPosef(cArray[i], cMemoryOwn);
		return arrayWrapper;
	}
}
