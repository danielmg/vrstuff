/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 3.0.2
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package danielmg.vr.jovr.jni;

public class OvrCapi implements OvrCapiConstants {
  public static char ovr_Initialize() {
    return OvrCapiJNI.ovr_Initialize();
  }

  public static void ovr_Shutdown() {
    OvrCapiJNI.ovr_Shutdown();
  }

  public static String ovr_GetVersionString() {
    return OvrCapiJNI.ovr_GetVersionString();
  }

  public static int ovrHmd_Detect() {
    return OvrCapiJNI.ovrHmd_Detect();
  }

  public static ovrHmdDesc ovrHmd_Create(int index) {
    long cPtr = OvrCapiJNI.ovrHmd_Create(index);
    return (cPtr == 0) ? null : new ovrHmdDesc(cPtr, false);
  }

  public static void ovrHmd_Destroy(ovrHmdDesc hmd) {
    OvrCapiJNI.ovrHmd_Destroy(ovrHmdDesc.getCPtr(hmd), hmd);
  }

  public static ovrHmdDesc ovrHmd_CreateDebug(ovrHmdType type) {
    long cPtr = OvrCapiJNI.ovrHmd_CreateDebug(type.swigValue());
    return (cPtr == 0) ? null : new ovrHmdDesc(cPtr, false);
  }

  public static String ovrHmd_GetLastError(ovrHmdDesc hmd) {
    return OvrCapiJNI.ovrHmd_GetLastError(ovrHmdDesc.getCPtr(hmd), hmd);
  }

  public static char ovrHmd_AttachToWindow(ovrHmdDesc hmd, SWIGTYPE_p_void window, ovrRecti destMirrorRect, ovrRecti sourceRenderTargetRect) {
    return OvrCapiJNI.ovrHmd_AttachToWindow(ovrHmdDesc.getCPtr(hmd), hmd, SWIGTYPE_p_void.getCPtr(window), ovrRecti.getCPtr(destMirrorRect), destMirrorRect, ovrRecti.getCPtr(sourceRenderTargetRect), sourceRenderTargetRect);
  }

  public static long ovrHmd_GetEnabledCaps(ovrHmdDesc hmd) {
    return OvrCapiJNI.ovrHmd_GetEnabledCaps(ovrHmdDesc.getCPtr(hmd), hmd);
  }

  public static void ovrHmd_SetEnabledCaps(ovrHmdDesc hmd, long hmdCaps) {
    OvrCapiJNI.ovrHmd_SetEnabledCaps(ovrHmdDesc.getCPtr(hmd), hmd, hmdCaps);
  }

  public static char ovrHmd_ConfigureTracking(ovrHmdDesc hmd, long supportedTrackingCaps, long requiredTrackingCaps) {
    return OvrCapiJNI.ovrHmd_ConfigureTracking(ovrHmdDesc.getCPtr(hmd), hmd, supportedTrackingCaps, requiredTrackingCaps);
  }

  public static void ovrHmd_RecenterPose(ovrHmdDesc hmd) {
    OvrCapiJNI.ovrHmd_RecenterPose(ovrHmdDesc.getCPtr(hmd), hmd);
  }

  public static ovrTrackingState ovrHmd_GetTrackingState(ovrHmdDesc hmd, double absTime) {
    return new ovrTrackingState(OvrCapiJNI.ovrHmd_GetTrackingState(ovrHmdDesc.getCPtr(hmd), hmd, absTime), true);
  }

  public static ovrSizei ovrHmd_GetFovTextureSize(ovrHmdDesc hmd, ovrEyeType eye, ovrFovPort fov, float pixelsPerDisplayPixel) {
    return new ovrSizei(OvrCapiJNI.ovrHmd_GetFovTextureSize(ovrHmdDesc.getCPtr(hmd), hmd, eye.swigValue(), ovrFovPort.getCPtr(fov), fov, pixelsPerDisplayPixel), true);
  }

  public static char ovrHmd_ConfigureRendering(ovrHmdDesc hmd, ovrRenderAPIConfig apiConfig, long distortionCaps, ovrFovPort[] eyeFovIn, ovrEyeRenderDesc[] eyeRenderDescOut) {
    return OvrCapiJNI.ovrHmd_ConfigureRendering(ovrHmdDesc.getCPtr(hmd), hmd, ovrRenderAPIConfig.getCPtr(apiConfig), apiConfig, distortionCaps, ovrFovPort.cArrayUnwrap(eyeFovIn), ovrEyeRenderDesc.cArrayUnwrap(eyeRenderDescOut));
  }

  public static ovrFrameTiming ovrHmd_BeginFrame(ovrHmdDesc hmd, long frameIndex) {
    return new ovrFrameTiming(OvrCapiJNI.ovrHmd_BeginFrame(ovrHmdDesc.getCPtr(hmd), hmd, frameIndex), true);
  }

  public static void ovrHmd_EndFrame(ovrHmdDesc hmd, ovrPosef[] renderPose, ovrTexture[] eyeTexture) {
    OvrCapiJNI.ovrHmd_EndFrame(ovrHmdDesc.getCPtr(hmd), hmd, ovrPosef.cArrayUnwrap(renderPose), ovrTexture.cArrayUnwrap(eyeTexture));
  }

  public static ovrPosef ovrHmd_GetEyePose(ovrHmdDesc hmd, ovrEyeType eye) {
    return new ovrPosef(OvrCapiJNI.ovrHmd_GetEyePose(ovrHmdDesc.getCPtr(hmd), hmd, eye.swigValue()), true);
  }

  public static ovrEyeRenderDesc ovrHmd_GetRenderDesc(ovrHmdDesc hmd, ovrEyeType eyeType, ovrFovPort fov) {
    return new ovrEyeRenderDesc(OvrCapiJNI.ovrHmd_GetRenderDesc(ovrHmdDesc.getCPtr(hmd), hmd, eyeType.swigValue(), ovrFovPort.getCPtr(fov), fov), true);
  }

  public static char ovrHmd_CreateDistortionMesh(ovrHmdDesc hmd, ovrEyeType eyeType, ovrFovPort fov, long distortionCaps, ovrDistortionMesh meshData) {
    return OvrCapiJNI.ovrHmd_CreateDistortionMesh(ovrHmdDesc.getCPtr(hmd), hmd, eyeType.swigValue(), ovrFovPort.getCPtr(fov), fov, distortionCaps, ovrDistortionMesh.getCPtr(meshData), meshData);
  }

  public static void ovrHmd_DestroyDistortionMesh(ovrDistortionMesh meshData) {
    OvrCapiJNI.ovrHmd_DestroyDistortionMesh(ovrDistortionMesh.getCPtr(meshData), meshData);
  }

  public static void ovrHmd_GetRenderScaleAndOffset(ovrFovPort fov, ovrSizei textureSize, ovrRecti renderViewport, ovrVector2f[] uvScaleOffsetOut) {
    OvrCapiJNI.ovrHmd_GetRenderScaleAndOffset(ovrFovPort.getCPtr(fov), fov, ovrSizei.getCPtr(textureSize), textureSize, ovrRecti.getCPtr(renderViewport), renderViewport, ovrVector2f.cArrayUnwrap(uvScaleOffsetOut));
  }

  public static ovrFrameTiming ovrHmd_GetFrameTiming(ovrHmdDesc hmd, long frameIndex) {
    return new ovrFrameTiming(OvrCapiJNI.ovrHmd_GetFrameTiming(ovrHmdDesc.getCPtr(hmd), hmd, frameIndex), true);
  }

  public static ovrFrameTiming ovrHmd_BeginFrameTiming(ovrHmdDesc hmd, long frameIndex) {
    return new ovrFrameTiming(OvrCapiJNI.ovrHmd_BeginFrameTiming(ovrHmdDesc.getCPtr(hmd), hmd, frameIndex), true);
  }

  public static void ovrHmd_EndFrameTiming(ovrHmdDesc hmd) {
    OvrCapiJNI.ovrHmd_EndFrameTiming(ovrHmdDesc.getCPtr(hmd), hmd);
  }

  public static void ovrHmd_ResetFrameTiming(ovrHmdDesc hmd, long frameIndex) {
    OvrCapiJNI.ovrHmd_ResetFrameTiming(ovrHmdDesc.getCPtr(hmd), hmd, frameIndex);
  }

  public static void ovrHmd_GetEyeTimewarpMatrices(ovrHmdDesc hmd, ovrEyeType eye, ovrPosef renderPose, ovrMatrix4f twmOut) {
    OvrCapiJNI.ovrHmd_GetEyeTimewarpMatrices(ovrHmdDesc.getCPtr(hmd), hmd, eye.swigValue(), ovrPosef.getCPtr(renderPose), renderPose, ovrMatrix4f.getCPtr(twmOut), twmOut);
  }

  public static ovrMatrix4f ovrMatrix4f_Projection(ovrFovPort fov, float znear, float zfar, char rightHanded) {
    return new ovrMatrix4f(OvrCapiJNI.ovrMatrix4f_Projection(ovrFovPort.getCPtr(fov), fov, znear, zfar, rightHanded), true);
  }

  public static ovrMatrix4f ovrMatrix4f_OrthoSubProjection(ovrMatrix4f projection, ovrVector2f orthoScale, float orthoDistance, float eyeViewAdjustX) {
    return new ovrMatrix4f(OvrCapiJNI.ovrMatrix4f_OrthoSubProjection(ovrMatrix4f.getCPtr(projection), projection, ovrVector2f.getCPtr(orthoScale), orthoScale, orthoDistance, eyeViewAdjustX), true);
  }

  public static double ovr_GetTimeInSeconds() {
    return OvrCapiJNI.ovr_GetTimeInSeconds();
  }

  public static double ovr_WaitTillTime(double absTime) {
    return OvrCapiJNI.ovr_WaitTillTime(absTime);
  }

  public static char ovrHmd_ProcessLatencyTest(ovrHmdDesc hmd, short[] rgbColorOut) {
    return OvrCapiJNI.ovrHmd_ProcessLatencyTest(ovrHmdDesc.getCPtr(hmd), hmd, rgbColorOut);
  }

  public static String ovrHmd_GetLatencyTestResult(ovrHmdDesc hmd) {
    return OvrCapiJNI.ovrHmd_GetLatencyTestResult(ovrHmdDesc.getCPtr(hmd), hmd);
  }

  public static void ovrHmd_GetHSWDisplayState(ovrHmdDesc hmd, ovrHSWDisplayState hasWarningState) {
    OvrCapiJNI.ovrHmd_GetHSWDisplayState(ovrHmdDesc.getCPtr(hmd), hmd, ovrHSWDisplayState.getCPtr(hasWarningState), hasWarningState);
  }

  public static char ovrHmd_DismissHSWDisplay(ovrHmdDesc hmd) {
    return OvrCapiJNI.ovrHmd_DismissHSWDisplay(ovrHmdDesc.getCPtr(hmd), hmd);
  }

  public static char ovrHmd_GetBool(ovrHmdDesc hmd, String propertyName, char defaultVal) {
    return OvrCapiJNI.ovrHmd_GetBool(ovrHmdDesc.getCPtr(hmd), hmd, propertyName, defaultVal);
  }

  public static char ovrHmd_SetBool(ovrHmdDesc hmd, String propertyName, char value) {
    return OvrCapiJNI.ovrHmd_SetBool(ovrHmdDesc.getCPtr(hmd), hmd, propertyName, value);
  }

  public static int ovrHmd_GetInt(ovrHmdDesc hmd, String propertyName, int defaultVal) {
    return OvrCapiJNI.ovrHmd_GetInt(ovrHmdDesc.getCPtr(hmd), hmd, propertyName, defaultVal);
  }

  public static char ovrHmd_SetInt(ovrHmdDesc hmd, String propertyName, int value) {
    return OvrCapiJNI.ovrHmd_SetInt(ovrHmdDesc.getCPtr(hmd), hmd, propertyName, value);
  }

  public static float ovrHmd_GetFloat(ovrHmdDesc hmd, String propertyName, float defaultVal) {
    return OvrCapiJNI.ovrHmd_GetFloat(ovrHmdDesc.getCPtr(hmd), hmd, propertyName, defaultVal);
  }

  public static char ovrHmd_SetFloat(ovrHmdDesc hmd, String propertyName, float value) {
    return OvrCapiJNI.ovrHmd_SetFloat(ovrHmdDesc.getCPtr(hmd), hmd, propertyName, value);
  }

  public static long ovrHmd_GetFloatArray(ovrHmdDesc hmd, String propertyName, float[] values, long arraySize) {
    return OvrCapiJNI.ovrHmd_GetFloatArray(ovrHmdDesc.getCPtr(hmd), hmd, propertyName, values, arraySize);
  }

  public static char ovrHmd_SetFloatArray(ovrHmdDesc hmd, String propertyName, float[] values, long arraySize) {
    return OvrCapiJNI.ovrHmd_SetFloatArray(ovrHmdDesc.getCPtr(hmd), hmd, propertyName, values, arraySize);
  }

  public static String ovrHmd_GetString(ovrHmdDesc hmd, String propertyName, String defaultVal) {
    return OvrCapiJNI.ovrHmd_GetString(ovrHmdDesc.getCPtr(hmd), hmd, propertyName, defaultVal);
  }

  public static char ovrHmd_SetString(ovrHmdDesc hmddesc, String propertyName, String value) {
    return OvrCapiJNI.ovrHmd_SetString(ovrHmdDesc.getCPtr(hmddesc), hmddesc, propertyName, value);
  }

}
