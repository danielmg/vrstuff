/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 3.0.2
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package danielmg.vr.jovr.jni;

public class ovrEyeRenderDesc {
	private long swigCPtr;
	protected boolean swigCMemOwn;

	protected ovrEyeRenderDesc(long cPtr, boolean cMemoryOwn) {
		swigCMemOwn = cMemoryOwn;
		swigCPtr = cPtr;
	}

	protected static long getCPtr(ovrEyeRenderDesc obj) {
		return (obj == null) ? 0 : obj.swigCPtr;
	}

	protected void finalize() {
		delete();
	}

	public synchronized void delete() {
		if (swigCPtr != 0) {
			if (swigCMemOwn) {
				swigCMemOwn = false;
				OvrCapiJNI.delete_ovrEyeRenderDesc(swigCPtr);
			}
			swigCPtr = 0;
		}
	}

	public void setEye(ovrEyeType value) {
		OvrCapiJNI.ovrEyeRenderDesc_Eye_set(swigCPtr, this, value.swigValue());
	}

	public ovrEyeType getEye() {
		return ovrEyeType.swigToEnum(OvrCapiJNI.ovrEyeRenderDesc_Eye_get(
				swigCPtr, this));
	}

	public void setFov(ovrFovPort value) {
		OvrCapiJNI.ovrEyeRenderDesc_Fov_set(swigCPtr, this,
				ovrFovPort.getCPtr(value), value);
	}

	public ovrFovPort getFov() {
		long cPtr = OvrCapiJNI.ovrEyeRenderDesc_Fov_get(swigCPtr, this);
		return (cPtr == 0) ? null : new ovrFovPort(cPtr, false);
	}

	public void setDistortedViewport(ovrRecti value) {
		OvrCapiJNI.ovrEyeRenderDesc_DistortedViewport_set(swigCPtr, this,
				ovrRecti.getCPtr(value), value);
	}

	public ovrRecti getDistortedViewport() {
		long cPtr = OvrCapiJNI.ovrEyeRenderDesc_DistortedViewport_get(swigCPtr,
				this);
		return (cPtr == 0) ? null : new ovrRecti(cPtr, false);
	}

	public void setPixelsPerTanAngleAtCenter(ovrVector2f value) {
		OvrCapiJNI.ovrEyeRenderDesc_PixelsPerTanAngleAtCenter_set(swigCPtr,
				this, ovrVector2f.getCPtr(value), value);
	}

	public ovrVector2f getPixelsPerTanAngleAtCenter() {
		long cPtr = OvrCapiJNI.ovrEyeRenderDesc_PixelsPerTanAngleAtCenter_get(
				swigCPtr, this);
		return (cPtr == 0) ? null : new ovrVector2f(cPtr, false);
	}

	public void setViewAdjust(ovrVector3f value) {
		OvrCapiJNI.ovrEyeRenderDesc_ViewAdjust_set(swigCPtr, this,
				ovrVector3f.getCPtr(value), value);
	}

	public ovrVector3f getViewAdjust() {
		long cPtr = OvrCapiJNI.ovrEyeRenderDesc_ViewAdjust_get(swigCPtr, this);
		return (cPtr == 0) ? null : new ovrVector3f(cPtr, false);
	}

	public ovrEyeRenderDesc() {
		this(OvrCapiJNI.new_ovrEyeRenderDesc(), true);
	}

	protected static long[] cArrayUnwrap(ovrEyeRenderDesc[] arrayWrapper) {
		long[] cArray = new long[arrayWrapper.length];
		for (int i = 0; i < arrayWrapper.length; i++)
			cArray[i] = ovrEyeRenderDesc.getCPtr(arrayWrapper[i]);
		return cArray;
	}

	protected static ovrEyeRenderDesc[] cArrayWrap(long[] cArray,
			boolean cMemoryOwn) {
		ovrEyeRenderDesc[] arrayWrapper = new ovrEyeRenderDesc[cArray.length];
		for (int i = 0; i < cArray.length; i++)
			arrayWrapper[i] = new ovrEyeRenderDesc(cArray[i], cMemoryOwn);
		return arrayWrapper;
	}

}
