/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 3.0.2
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package danielmg.vr.jovr.jni;

public class ovrVector2f {
	private long swigCPtr;
	protected boolean swigCMemOwn;

	protected ovrVector2f(long cPtr, boolean cMemoryOwn) {
		swigCMemOwn = cMemoryOwn;
		swigCPtr = cPtr;
	}

	protected static long getCPtr(ovrVector2f obj) {
		return (obj == null) ? 0 : obj.swigCPtr;
	}

	protected void finalize() {
		delete();
	}

	public synchronized void delete() {
		if (swigCPtr != 0) {
			if (swigCMemOwn) {
				swigCMemOwn = false;
				OvrCapiJNI.delete_ovrVector2f(swigCPtr);
			}
			swigCPtr = 0;
		}
	}

	public void setX(float value) {
		OvrCapiJNI.ovrVector2f_x_set(swigCPtr, this, value);
	}

	public float getX() {
		return OvrCapiJNI.ovrVector2f_x_get(swigCPtr, this);
	}

	public void setY(float value) {
		OvrCapiJNI.ovrVector2f_y_set(swigCPtr, this, value);
	}

	public float getY() {
		return OvrCapiJNI.ovrVector2f_y_get(swigCPtr, this);
	}

	public ovrVector2f() {
		this(OvrCapiJNI.new_ovrVector2f(), true);
	}

	protected static long[] cArrayUnwrap(ovrVector2f[] arrayWrapper) {
		long[] cArray = new long[arrayWrapper.length];
		for (int i = 0; i < arrayWrapper.length; i++)
			cArray[i] = ovrVector2f.getCPtr(arrayWrapper[i]);
		return cArray;
	}

	protected static ovrVector2f[] cArrayWrap(long[] cArray, boolean cMemoryOwn) {
		ovrVector2f[] arrayWrapper = new ovrVector2f[cArray.length];
		for (int i = 0; i < cArray.length; i++)
			arrayWrapper[i] = new ovrVector2f(cArray[i], cMemoryOwn);
		return arrayWrapper;
	}
}
