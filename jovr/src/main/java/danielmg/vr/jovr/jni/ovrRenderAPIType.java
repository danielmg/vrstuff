/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 3.0.2
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package danielmg.vr.jovr.jni;

public final class ovrRenderAPIType {
  public final static ovrRenderAPIType ovrRenderAPI_None = new ovrRenderAPIType("ovrRenderAPI_None");
  public final static ovrRenderAPIType ovrRenderAPI_OpenGL = new ovrRenderAPIType("ovrRenderAPI_OpenGL");
  public final static ovrRenderAPIType ovrRenderAPI_Android_GLES = new ovrRenderAPIType("ovrRenderAPI_Android_GLES");
  public final static ovrRenderAPIType ovrRenderAPI_D3D9 = new ovrRenderAPIType("ovrRenderAPI_D3D9");
  public final static ovrRenderAPIType ovrRenderAPI_D3D10 = new ovrRenderAPIType("ovrRenderAPI_D3D10");
  public final static ovrRenderAPIType ovrRenderAPI_D3D11 = new ovrRenderAPIType("ovrRenderAPI_D3D11");
  public final static ovrRenderAPIType ovrRenderAPI_Count = new ovrRenderAPIType("ovrRenderAPI_Count");

  public final int swigValue() {
    return swigValue;
  }

  public String toString() {
    return swigName;
  }

  public static ovrRenderAPIType swigToEnum(int swigValue) {
    if (swigValue < swigValues.length && swigValue >= 0 && swigValues[swigValue].swigValue == swigValue)
      return swigValues[swigValue];
    for (int i = 0; i < swigValues.length; i++)
      if (swigValues[i].swigValue == swigValue)
        return swigValues[i];
    throw new IllegalArgumentException("No enum " + ovrRenderAPIType.class + " with value " + swigValue);
  }

  private ovrRenderAPIType(String swigName) {
    this.swigName = swigName;
    this.swigValue = swigNext++;
  }

  private ovrRenderAPIType(String swigName, int swigValue) {
    this.swigName = swigName;
    this.swigValue = swigValue;
    swigNext = swigValue+1;
  }

  private ovrRenderAPIType(String swigName, ovrRenderAPIType swigEnum) {
    this.swigName = swigName;
    this.swigValue = swigEnum.swigValue;
    swigNext = this.swigValue+1;
  }

  private static ovrRenderAPIType[] swigValues = { ovrRenderAPI_None, ovrRenderAPI_OpenGL, ovrRenderAPI_Android_GLES, ovrRenderAPI_D3D9, ovrRenderAPI_D3D10, ovrRenderAPI_D3D11, ovrRenderAPI_Count };
  private static int swigNext = 0;
  private final int swigValue;
  private final String swigName;
}

