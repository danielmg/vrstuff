 %module OvrCapi

%include "carrays.i"
%include "arrays_java.i"
%include "typemaps.i"

 		  
 %{	
 /* Includes the header in the wrapper code */
 #include "OVR_CAPI.h"
 %}
  
	
JAVA_ARRAYSOFCLASSES(ovrFovPort);
JAVA_ARRAYSOFCLASSES(ovrEyeRenderDesc);
JAVA_ARRAYSOFCLASSES(ovrVector2f);
JAVA_ARRAYSOFCLASSES(ovrPosef);
JAVA_ARRAYSOFCLASSES(ovrTexture);



/* Parse the header file to generate wrappers */
 %include "OVR_CAPI.h"

 ovrBool ovrHmd_ConfigureRendering( ovrHmd hmd, const ovrRenderAPIConfig* apiConfig,
                                              unsigned int distortionCaps,
                                              const ovrFovPort eyeFovIn[2],
                                              ovrEyeRenderDesc eyeRenderDescOut[2] );
void     ovrHmd_EndFrame(ovrHmd hmd,
                                    const ovrPosef renderPose[2],
                                    const ovrTexture eyeTexture[2]);


void     ovrHmd_GetRenderScaleAndOffset( ovrFovPort fov,
                                                    ovrSizei textureSize, ovrRecti renderViewport,
                                                    ovrVector2f uvScaleOffsetOut[2] );

	%extend ovrMatrix4f_ {
	  float getM(int i, int j) {
		return $self->M[i][j];
	  }
	}
					
/*

I'm doing something wrong as I can't get it to create the cArrayUnwrap methods on the proxy classes.  So I need manually do this code to each one:

 protected static long[] cArrayUnwrap($javaclassname[] arrayWrapper) {
   long[] cArray = new long[arrayWrapper.length];
      for (int i=0; i<arrayWrapper.length; i++)
        cArray[i] = $javaclassname.getCPtr(arrayWrapper[i]);
      return cArray;
  }

  protected static $javaclassname[] cArrayWrap(long[] cArray, boolean cMemoryOwn) {
    $javaclassname[] arrayWrapper = new $javaclassname[cArray.length];
    for (int i=0; i<cArray.length; i++)
      arrayWrapper[i] = new $javaclassname(cArray[i], cMemoryOwn);
    return arrayWrapper;
  }

*/									