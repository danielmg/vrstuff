 %module OvrCapiD3D

 %{
 /* Includes the header in the wrapper code */
 #include "OVR_CAPI_D3D .h"
 %}

 %include "arrays_java.i"

 /* Parse the header file to generate wrappers */
 %include "OVR_CAPI_D3D .h"
